import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import {MatSelectModule} from '@angular/material/select'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog'; 
import { FormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button'; 
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {PanelModule} from 'primeng/panel';
import { ReactiveFormsModule } from '@angular/forms';
import {TableModule} from 'primeng/table';
import { EditableTableModule } from 'angular-inline-editable-table'

import { HttpClientModule } from '@angular/common/http';
import {AngularTreeGridModule} from 'angular-tree-grid';
import {TreeTableModule} from 'primeng/treetable';
import {CardModule} from 'primeng/card';
import { TreeModule } from 'primeng/tree';
import {SliderModule} from 'primeng/slider';
import {PanelMenuModule} from 'primeng/panelmenu';
import {ContextMenuModule} from 'primeng/contextmenu';
import { FormComponent } from './form/form.component';
import {MenubarModule} from 'primeng/menubar';
import {ToastModule} from 'primeng/toast';
import { Form1Component } from './form1/form1.component';
@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    Form1Component


   

 
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    ButtonModule,
    DialogModule,
    MessageModule,
    MessagesModule,
    DropdownModule,
    PanelModule,
    ReactiveFormsModule,
    TableModule,
    AngularTreeGridModule,
    EditableTableModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TreeTableModule,
    CardModule,
    TreeModule,
    PanelMenuModule,
    MenubarModule,
    ToastModule,
    ContextMenuModule,
    SliderModule
    
    

    
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
