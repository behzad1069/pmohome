import { Router } from '@angular/router';


import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { SelectItem } from 'primeng/api/selectitem';
import { MessageService, MenuItem } from 'primeng/api/';
@Component({
    selector: 'form1-root',
    templateUrl: './form1.component.html',
    styleUrls: ['./form1.component.scss'],
    providers: [MessageService],
   
  })
  export class Form1Component  {
    selectedCompany:any
    disable=false
    visible: boolean;
    submitted: boolean;
    personnel=[];
    dialogForm: FormGroup;
    description: string;
    userData:boolean=false;
   
    enableEditIndex = null;
    companyDefinitions: FormGroup;
    displayBasic=false
    companyTypes: SelectItem[];
    openMenu=true;
    showTable: boolean;
    tableHeaders = ['نام کامل', 'کد ملی', 'تلفن همراه','ایمیل'];
    tableRowsWithId: any[][] = [];
    State: any[];
    States: any[];
    TableList:[];
    officersIds: string;
    
    stateTypes: any[];
    CompanyTypes: any[];
    cityFilter: any[];
    items: MenuItem[];
    
    guyg: any;
    constructor(private fb: FormBuilder, private messageService: MessageService,private router:Router) {}
    ngOnInit() {
  
      this.items = [
        {
            label: '',
            icon: 'pi pi-ellipsis-v',
            items: [
                {label: 'Delete', icon: 'pi pi-fw pi-trash',  command: (event: Event) => this.remove(this.guyg)},
                {label: 'edit', icon: 'pi pi-fw pi-pencil', command: (event: Event) => this.editTable(this.selectedCompany)}  
            ]
        },
        {separator:true},
    ];
  
        this.companyDefinitions = this.fb.group({
          'company_type': new FormControl('', Validators.required),
          'name': new FormControl('', Validators.required),
          'company_registration_code': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
          'manager_full_name': new FormControl('', Validators.required), 
          'city': new FormControl('', Validators.required),
          'tell': new FormControl('', [Validators.pattern('^(((\\+|00)?98)|0)?9\\d{9}$'), Validators.required]),
          'address': new FormControl('', Validators.required),
          'province': new FormControl('', Validators.required)
        });
        this.dialogForm = this.fb.group({
          'first_name': new FormControl('', Validators.required),
          'national_code': new FormControl('', Validators.compose([Validators.required,this.CustomNationalCodeValidator])),
          'tell': new FormControl('', [Validators.pattern('^(((\\+|00)?98)|0)?9\\d{9}$'), Validators.required]),
          'email': new FormControl('', (Validators.required,Validators.email)),
          'last_name': new FormControl(''),
          
      });
  

   
        this.CompanyTypes = [];
        this.CompanyTypes.push({label:'کنترل پروژه', value:'کنترل پروژه'});
        this.CompanyTypes.push({label:'ناظر', value:'ناظر'});
    
  
     if(this.personnel){
      this.showTable= true;
     };
    }
  
    dataType(tableHeaders: string[], tableRowsWithId: any[][], dataType: any) {
      throw new Error("Method not implemented.");
    }
  
    clickToggle(event){
      this.openMenu = !this.openMenu ;
    }
    
    addRow() {
      this.visible = true;
      this.userData=true;
    }
  
    showBasicDialog() {
      this.displayBasic = true;
  }
  
    onSubmit(i) {
      this.submitted = true;
      this.messageService.add({severity:'info', summary:'Success', detail:'Form Submitted'});
      this.showTable = false; 
    }
  
    registerDialog(){
      this.personnel.push(this.dialogForm.value);
      this.dialogForm.reset();
    }
   
    remove(c){
      const index =this.personnel.indexOf(c);
      this.personnel.splice(index, 1);
    }
  
    editTable(i){
      this.disable = true;
      this.enableEditIndex = i;
    }
  
    private fillRegister(): any {
      return {
        company_type:this.companyDefinitions.value.company_type,
        name:this.companyDefinitions.value.name,
        company_registration_code:this.companyDefinitions.value.company_registration_code,
        manager_full_name:this.companyDefinitions.value.manager_full_name,
        province:this.companyDefinitions.value.province,
        city:this.companyDefinitions.value.city,
        tell:this.companyDefinitions.value.tell,
        address:this.companyDefinitions.value.address,
        personnel:this.personnel,
      }; 
    }  
    cancelTable(){
      this.disable = false;
    }
  
    menuItem(company,i){
      this.selectedCompany = company;
      this.guyg=i
    }
  
    saveTable(){
      this.disable = false;
    }
  
  
  
//    clearFilter(c){
//     const titleProvinces = Provinces.filter(i => i.title === c);
//     const idProvinces = titleProvinces.map(officer => officer.id);
//     const empires = Cities.filter(i => i.province_id === idProvinces[0]);
//     const officersIdss = empires.map(i => i.title);
//     this.cityFilter = [];
//     for(let i of officersIdss)
//     this.cityFilter.push({label:i,value:i});
//   }
  
   CustomNationalCodeValidator(control:AbstractControl){
    const nationalId = /^\d{10}$/;
    const validCount = nationalId.test(control.value);
    var check = parseInt(control.value[9]);
    console.log(check)
    var sum = 0;
    var i;
    for (i = 0; i < 9; ++i) {
      sum += parseInt(control.value[i]) * (10 - i);
    }
    sum %= 11;
    const validSum = (sum < 2 && check == sum) || (sum >= 2 && check + sum == 11);
    const valid = validCount && validSum;
    return valid ? null : { appNationalIdValidator: true };
  
   }
  
  }
  