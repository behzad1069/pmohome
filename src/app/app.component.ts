import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
 
})
export class AppComponent implements OnInit {
  items=[];
  itemBar=[]
ngOnInit(){


  this.itemBar = [
 
    {
        label: '',
        
        items: [
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Save', icon: 'pi pi-fw pi-save'},
                    {label: 'Update', icon: 'pi pi-fw pi-save'},
                ]
            },
            {
                label: 'Other',
                icon: 'pi pi-fw pi-tags',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-minus'}
                ]
            }
        ]
    },
];




  this.items = [
    {
        label: 'اطلاعات پایه',
        items: [{
                label: ' لیست شرکت ها', 
                command: () => alert('item 1 1'),

            },
            {label: 'تعریف نوع فعالیت '},
            {separator: true},
            {label: 'issue تعریف نوع  '},
            {label: 'hold تعریف نوع  '}
        ]
    },
    {
        label: 'مدیریت پروژه ',
        items: [
            {label: 'Delete', icon: 'pi pi-fw pi-trash'},
            {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
        ]
    },
    {
        label: 'کارتابل',
        
        items: [
            {
                label: 'Contents',
                icon: 'pi pi-pi pi-bars'
            },
            {
                label: 'Search', 
                icon: 'pi pi-pi pi-search', 
                items: [
                    {
                        label: 'Text', 
                        items: [
                            {
                                label: 'Workspace'
                            }
                        ]
                    },
                    {
                        label: 'User',
                        icon: 'pi pi-fw pi-file',
                    }
            ]}
        ]
    },
    {
        label: 'گزارشات',
        items: [
            {
                label: 'Edit',
                icon: 'pi pi-fw pi-pencil',
                items: [
                    {label: 'Save', icon: 'pi pi-fw pi-save'},
                    {label: 'Update', icon: 'pi pi-fw pi-save'},
                ]
            },
            {
                label: 'Other',
                icon: 'pi pi-fw pi-tags',
                items: [
                    {label: 'Delete', icon: 'pi pi-fw pi-minus'}
                ]
            }
        ]
    }
];
}
}


